# frozen_string_literal: true

require 'puppet/resource_api'

Puppet::ResourceApi.register_type(
  name: 'sysrc',
  docs: <<-EOS,
@summary a type to query and set rc.conf via sysrc
@example
sysrc { 'foo_enable':
  ensure => 'present',
  value  => 'YES',
}

This type provides Puppet with the capabilities to manage BSD style rc.conf(5) files, using the sysrc(8) utility.
EOS
  features: [],
  attributes: {
    ensure: {
      type:    'Enum[present, absent]',
      desc:    'Whether this resource should be present or absent on the target system.',
      default: 'present',
    },
    name: {
      type:      'String',
      desc:      'The name of the resource you want to manage.',
      behaviour: :namevar,
    },
    value: {
      type:      'String',
      desc:      'The value of the resource you want to manage.',
    },
  },
)
