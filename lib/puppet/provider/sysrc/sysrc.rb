# frozen_string_literal: true

require 'puppet/resource_api/simple_provider'
require 'puppet/util/execution'

# Implementation for the sysrc type using the Resource API.
class Puppet::Provider::Sysrc::Sysrc < Puppet::ResourceApi::SimpleProvider
  def get(context)
    context.debug('Getting all configured rc.conf values')
    rc_conf = Puppet::Util::Execution.execute(
      [
        'sysrc',
        '-a', # Dump a list of all non-default configuration variables.
        '-e', # use `=` to dump the key=values
      ],
      failonfail: true,
    )
    require 'shellwords'
    @resource_hash = rc_conf.split("\n").map do |kv|
      name, value = kv.split('=', 2)

      {
        ensure: 'present',
        name: name,
        value: Shellwords.split(value).join(' '),
      }
    end
  end

  def create(context, name, should)
    context.notice("Creating '#{name}' with #{should.inspect}")

    value = Shellwords.split(should[:value]).join(' ')
    Puppet::Util::Execution.execute(
      [
        'sysrc',
        '-q',
        "#{name}=#{value}",
      ],
      failonfail: true,
    )
  end

  def update(context, name, should)
    context.notice("Updating '#{name}' with #{should.inspect}")
    value = Shellwords.split(should[:value]).join(' ')
    Puppet::Util::Execution.execute(
      [
        'sysrc',
        '-q',
        "#{name}=#{value}",
      ],
      failonfail: true,
    )
  end

  def delete(context, name)
    context.notice("Deleting '#{name}'")
    Puppet::Util::Execution.execute(
      [
        'sysrc',
        '-qx',
        name,
      ],
      failonfail: true,
    )
  end
end
