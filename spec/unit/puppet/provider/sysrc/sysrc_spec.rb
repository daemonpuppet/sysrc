# frozen_string_literal: true

require 'spec_helper'

ensure_module_defined('Puppet::Provider::Sysrc')
require 'puppet/provider/sysrc/sysrc'

RSpec.describe Puppet::Provider::Sysrc::Sysrc do
  subject(:provider) { described_class.new }

  let(:rc_conf) do
    f = <<-RC_CONF
clear_tmp_enable="YES"
syslogd_flags="-ss"
sendmail_enable="NONE"
hostname="fbsd12-1"
ifconfig_vtnet0="DHCP"
ifconfig_vtnet0_ipv6="inet6 accept_rtadv"
sshd_enable="YES"
ntpd_enable="YES"
dumpdev="AUTO"
zfs_enable="YES"
libvirtd_enable="YES"
bsdstats_enable="NO"
RC_CONF
    f
  end

  let(:rc) do
    [
      {
        name: 'bsdstats_enable',
        ensure: 'present',
        value: 'NO',
      },
      {
        name: 'clear_tmp_enable',
        ensure: 'present',
        value: 'YES',
      },
      {
        name: 'dumpdev',
        ensure: 'present',
        value: 'AUTO',
      },
      {
        name: 'hostname',
        ensure: 'present',
        value: 'fbsd12-1',
      },
      {
        name: 'ifconfig_vtnet0',
        ensure: 'present',
        value: 'DHCP',
      },
      {
        name: 'ifconfig_vtnet0_ipv6',
        ensure: 'present',
        value: 'inet6 accept_rtadv',
      },
      {
        name: 'libvirtd_enable',
        ensure: 'present',
        value: 'YES',
      },
      {
        name: 'ntpd_enable',
        ensure: 'present',
        value: 'YES',
      },
      {
        name: 'sendmail_enable',
        ensure: 'present',
        value: 'NONE',
      },
      {
        name: 'sshd_enable',
        ensure: 'present',
        value: 'YES',
      },
      {
        name: 'syslogd_flags',
        ensure: 'present',
        value: '-ss',
      },
      {
        name: 'zfs_enable',
        ensure: 'present',
        value: 'YES',
      },
    ]
  end

  let(:context) { instance_double('Puppet::ResourceApi::BaseContext', 'context') }

  before(:each) do
    allow(Puppet::Util::Execution).to receive(:execute).and_return rc_conf
  end

  describe '#get' do
    it 'processes resources' do
      expect(context).to receive(:debug).with('Getting all configured rc.conf values')
      provider.get(context).each do |rv|
        expect(rc).to include rv
      end
    end
  end

  describe 'create(context, name, should)' do
    it 'creates the resource' do
      expect(context).to receive(:notice).with(%r{\ACreating 'foo'})
      expect(Puppet::Util::Execution).to receive(:execute).with(['sysrc', '-q', "foo=bar"], failonfail: true)
      provider.create(context, 'foo', name: 'foo', value: 'bar', ensure: 'present')
    end
  end

  describe 'update(context, name, should)' do
    it 'updates the resource' do
      expect(context).to receive(:notice).with(%r{\AUpdating 'foo'})
      expect(Puppet::Util::Execution).to receive(:execute).with(['sysrc', '-q', "foo=zomg"], failonfail: true)
      provider.update(context, 'foo', name: 'foo', value: 'zomg', ensure: 'present')
    end
  end

  describe 'delete(context, name)' do
    it 'deletes the resource' do
      expect(context).to receive(:notice).with(%r{\ADeleting 'foo'})
      expect(Puppet::Util::Execution).to receive(:execute).with(['sysrc', '-qx', "foo"], failonfail: true)
      provider.delete(context, 'foo')
    end
  end
end
