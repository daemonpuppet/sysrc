# frozen_string_literal: true

require 'spec_helper'
require 'puppet/type/sysrc'

RSpec.describe 'the sysrc type' do
  it 'loads' do
    expect(Puppet::Type.type(:sysrc)).not_to be_nil
  end
end
